# Solusi Soal Nomor 2 - Tes PGN Role QA

## A. Test Case Withdrawal

**Test case name**: Beta-2.0-Site.Test-Withdrawal

**Test case type**: Functional test case

**Requirement no**: 2

**Module**: Withdrawal

**Status**: XXX

**Severity**: Critical

**Release**: Beta

**Version**: 2

**Pre-condition**:

- The ATM machine is functioning properly

- User has valid ATM card and PIN

- Sufficient funds available in Bank A account for withdrawal.

**Test data**:

- PIN-123456

- Valid amount-500000

- Invalid amount-501000000

**Summary**: To check the withdrawal process functionality.

| Step No | Description                                                              | Input     | Expected Result                          | Actual Result   | Status | Comments |
|---------|--------------------------------------------------------------------------|-----------|------------------------------------------|-----------------|--------|----------|
| 1       | Insert ATM card into ATM machine slot                                    | -         | ATM machine prompts for PIN              | As expected     | Pass   | XXX      |
| 2       | Enter the the following PIN                                              | 123456    | Accpect                                  | As expected     | Pass   | XXX      |
| 3       | Select the **Withdraw Cash** option from the ATM menu                    | -         | ATM display options for cash withdrawal  | As expected     | Pass   | XXX      |
| 4       | Enter the following amount to withdraw                                   |           |                                          |                 |        |          |
|         | Valid                                                                    | 500000    | Accpect                                  | As expected     | Pass   | XXX      |
|         | Invalid                                                                  | 501000000 | ATM displays message: Insufficient funds | Not as expected | Fail   |          |
| 5       | Confirm the transaction details and proceed                              | -         | ATM prompts for confirmation             | As expected     | Pass   | XXX      |
| 6       | Verify the transaction confirmation receipt printed by the ATM machine   | -         | Transaction details must be printed      | As expected     | Pass   | XXX      |
| 7       | Check the account balance to ensure the deducted amount has been updated | -         | Updated balance must be reflected        | As expected     | Pass   | XXX      |

**Author**: John Doe

**Date**: 20/03/2024

**Reviewed By**: Jane Doe

**Approved By**: Mike Smith

## B. Test Case Transfer

### **Functional Test Case**

**Test case name**: Beta-2.0-Site.Test-Amount Transfer

**Test case type**: Functional test case

**Requirement no**: 3

**Module**: Amount Transfer

**Status**: XXX

**Severity**: Critical

**Release**: Beta

**Version**: 2

**Pre-condition**:

- The ATM machine is functioning properly

- User has valid ATM card and PIN

- Sufficient funds available in Bank A account for the transfer.

**Test data**:

- PIN-123456

- Recipient account number-0987654321

- Valid amount-500000

- Invalid amount-501000000

**Summary**: To check amount transfer functionality

| Step No | Description                                                              | Input      | Expected Result                                            | Actual Results  | Status | Comments |
|---------|--------------------------------------------------------------------------|------------|------------------------------------------------------------|-----------------|--------|----------|
| 1       | Insert ATM card into the ATM machine slot                                | -          | ATM machine prompts for PIN                                | As expected     | Pass   | XXX      |
| 2       | Enter following for PIN                                                  | -          |                                                            |                 |        |          |
|         | Valid                                                                    | 123456     | **Accpect**                                                | As expected     | Pass   | XXX      |
|         | Invalid                                                                  | 000000     | ATM machine re-asking for PIN                              | Not as expected | Fail   |          |
| 3       | Select the "Transfer Funds" option from the menu                         | -          | ATM displays options for fund transfer                     | As Expected     | Pass   | XXX      |
| 4       | Enter the recipient account number (Bank B account number)               | 0987654321 | **Accpect**                                                | As expected     | Pass   | XXX      |
| 5       | Enter the following for amount to transfer                               |            |                                                            |                 |        |          |
|         | Valid                                                                    | 500000     | **Accpect**                                                | As expected     | Pass   | XXX      |
|         | Invalid                                                                  | 501000000  | ATM displays message: Amount should be between 5000-500000 | Not as expected | Fail   |          |
| 6       | Confirm the transaction details and proceed                              | -          | ATM prompts for confirmation                               | As expected     | Pass   | XXX      |
| 7       | Verify the transaction confirmation receipt printed by the ATM machine   | -          | Transaction details must be printed                        | As expected     | Pass   | XXX      |
| 8       | Check the account balance to ensure the deducted amount has been updated | -          | Updated balance must be reflected                          | As expected     | Pass   | XXX      |


**Author**: John Doe

**Date**: 20/03/2024

**Reviewed By**: Jane Doe

**Approved By**: Mike Smith
