# Solusi Soal Nomor 1 - Tes PGN Role QA

## A. Test Case Login Page

### **Functional Test Case**

**Test case name**: Beta-2.0-Site.Test-Login

**Test case type**: Functional test case

**Requirement no**: 1

**Module**: Login

**Status**: XXX

**Severity**: Critical

**Release**: Beta

**Version**: 2

**Pre-condition**: Required one login

**Test data**: username-admin, password-123456

**Summary**: To check login functionality

| **Steps No** | **Description**                                                                                                                                                                                                               | **Inputs**              | **Expected Result**                        | **Actual Results** | **Status** | **Comments** |
|--------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|--------------------------------------------|--------------------|------------|--------------|
| **1**        | Open **Browser** and enter the **Url**                                                                                                                                                                                        | https://site.test/login | **Login page** must be displayed           | As expected        | Pass       | XXX          |
| **2**        | Enter the following values for **Username**:                                                                                                                                                                                  |                         |                                            |                    |            |              |
|              | Valid                                                                                                                                                                                                                         | admin                   | **Accpect**                                | As expected        | Pass       | XXX          |
|              | Invalid                                                                                                                                                                                                                       | 666                     | Error message: "Invalid login credentials" | Not as expected    | Fail       |              |
|              | Blank                                                                                                                                                                                                                         |                         | Error message: "Username cannot be empty"  | Not as expected    | Fail       |              |
|              | Symbols                                                                                                                                                                                                                       | 2 alphabet              | Error message: "Invalid login credentials" | Not as expected    | Fail       |              |
| **3**        | Enter the following values for **Password**:                                                                                                                                                                                  |                         |                                            |                    |            |              |
|              | Valid                                                                                                                                                                                                                         |                         | **Accpect**                                | As expected        | Pass       | XXX          |
|              | Invalid                                                                                                                                                                                                                       | xxx                     | Error message: "Invalid login credentials" | Not as expected    | Fail       |              |
|              | Blank                                                                                                                                                                                                                         |                         | Error message: "Password cannot be empty"  | Not as expected    | Fail       |              |
| **4**        | Enter valid **Username** and **Password** and then click **Submit** button                                                                                                                                                    | admin, 123456           | **Homepage** must be displayed             | As expected        | Pass       | XXX          |
| **5**        | Enter valid **Username** and **Password**, turn on **Remember me** radio button, click **Submit** button. After successfully login and go to homepage, close the **Browser** and reopen the **Browser** and go to the **Url** | https://site.test/login | **Homepage** must be displayed             | As expected        | Pass       | XXX          |

**Author**: John Doe

**Date**: 20/03/2024

**Reviewed By**: Jane Doe

**Approved By**: Mike Smith

## B. Automated Test For Login Page

First initialize Selenium WebDriver

    // Start a Selenium WebDriver server
    const (
        seleniumPath     = "c:/sample/path/selenium-server.jar"
        port             = 4444
        chromeDriverPath = "c:/example/path/chromedriver"
    )

    opts := []selenium.ServiceOption{}
    service, err := selenium.NewChromeDriverService(chromeDriverPath, port, opts...)
    if err != nil {
        fmt.Println("Error starting the ChromeDriver server:", err)
        return
    }
    defer service.Stop()

    // Set up Chrome options
    chromeCaps := selenium.Capabilities{
        "args": []string{"--headless", "--disable-gpu", "--no-sandbox"},
    }

    // Connect to the WebDriver instance running locally
    wd, err := selenium.NewRemote(chromeCaps, fmt.Sprintf("http://localhost:%d/wd/hub", port))
    if err != nil {
        fmt.Println("Failed to open session:", err)
        return
    }
    defer wd.Quit()

Here we navigate to the login page:

    // Define the URL of the login page
    url := "https://site.test/login"

    // Open the login page
    if err := wd.Get(url); err != nil {
        fmt.Println("Error opening the login page:", err)
        return
    }

Locate the web elements which includes steps: finding username and password fields, use CSS selectors to locate the elements:

    // Find username and password fields and submit button
    usernameField, err := wd.FindElement(selenium.ByCSSSelector, "input[name='username']")
    if err != nil {
        fmt.Println("Error finding username field:", err)
        return
    }
    passwordField, err := wd.FindElement(selenium.ByCSSSelector, "input[name='password']")
    if err != nil {
        fmt.Println("Error finding password field:", err)
        return
    }
    submitButton, err := wd.FindElement(selenium.ByCSSSelector, "input[type='submit']")
    if err != nil {
        fmt.Println("Error finding submit button:", err)
        return
    }

Perform actions on the located Web Elements:

    // Input username and password
    usernameField.SendKeys(data.username)
    passwordField.SendKeys(data.password)

    // Click the submit button
    submitButton.Click()

Verify and validate the action:

    // Check expected result
    switch data.expectedResult {
    case "Success":
        // Check if redirected to homepage
        if _, err := wd.FindElement(selenium.ByCSSSelector, "body.homepage"); err != nil {
            fmt.Println("Test Failed: Expected redirection to homepage but not redirected")
            return
        }
        fmt.Println("Test Passed: Successful login")
    case "Failure":
        // Check if error message is displayed
        errorMessage, err := wd.FindElement(selenium.ByCSSSelector, "#error-message")
        if err != nil {
            fmt.Println("Test Failed: Expected error message not displayed")
            return
        }
        if text, err := errorMessage.Text(); err != nil || text != "Invalid login credentials" {
            fmt.Println("Test Failed: Expected error message not displayed")
            return
        }
        fmt.Println("Test Passed: Error message displayed for invalid credentials")
    }

Complete implementation code can be found [here](/soal_1/soal_1.go)

