package main

import (
	"fmt"
	"log"
	"time"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

func main() {
	// Start a Selenium WebDriver service for Chrome
	service, err := selenium.NewChromeDriverService("./chromedriver", 4444)
	if err != nil {
		log.Fatal("Error starting the ChromeDriver service:", err)
	}
	defer service.Stop()

	// Configure Chrome options
	caps := selenium.Capabilities{}
	chromeCaps := chrome.Capabilities{
		Args: []string{
			"--headless",
			"--disable-gpu",
			"--no-sandbox",
		},
	}
	caps.AddChrome(chromeCaps)

	// Create a new remote WebDriver client with the specified options
	driver, err := selenium.NewRemote(caps, "")
	if err != nil {
		log.Fatal("Error creating remote WebDriver:", err)
	}
	defer driver.Quit()

	// Define the URL of the login page
	url := "https://site.test/login"

	// Define test data
	testData := []struct {
		username       string
		password       string
		expectedResult string
	}{
		{"admin", "123456", "Success"},
		{"666", "invalid", "Failure"},
		{"", "password", "Failure"},
		{"xxx", "password", "Failure"},
		{"admin", "xxx", "Failure"},
		{"admin", "", "Failure"},
	}

	// Iterate through test data
	for _, data := range testData {
		// Open the login page
		if err := driver.Get(url); err != nil {
			log.Println("Error opening the login page:", err)
			return
		}
		time.Sleep(2 * time.Second) // Let the page load

		// Find username and password fields and submit button
		usernameField, err := driver.FindElement(selenium.ByCSSSelector, "input[name='username']")
		if err != nil {
			log.Println("Error finding username field:", err)
			return
		}
		passwordField, err := driver.FindElement(selenium.ByCSSSelector, "input[name='password']")
		if err != nil {
			log.Println("Error finding password field:", err)
			return
		}
		submitButton, err := driver.FindElement(selenium.ByCSSSelector, "input[type='submit']")
		if err != nil {
			log.Println("Error finding submit button:", err)
			return
		}

		// Input username and password
		if err := usernameField.SendKeys(data.username); err != nil {
			log.Println("Error sending username:", err)
			return
		}
		if err := passwordField.SendKeys(data.password); err != nil {
			log.Println("Error sending password:", err)
			return
		}

		// Click the submit button
		if err := submitButton.Click(); err != nil {
			log.Println("Error clicking submit button:", err)
			return
		}

		// Wait for the page to load
		time.Sleep(2 * time.Second)

		// Check expected result
		switch data.expectedResult {
		case "Success":
			// Check if redirected to homepage
			_, err := driver.FindElement(selenium.ByCSSSelector, "body.homepage")
			if err != nil {
				log.Println("Test Failed: Expected redirection to homepage but not redirected")
				return
			}
			fmt.Println("Test Passed: Successful login")
		case "Failure":
			// Check if error message is displayed
			errorMessage, err := driver.FindElement(selenium.ByCSSSelector, "#error-message")
			if err != nil {
				log.Println("Test Failed: Expected error message not displayed")
				return
			}
			text, err := errorMessage.Text()
			if err != nil || text != "Invalid login credentials" {
				log.Println("Test Failed: Expected error message not displayed")
				return
			}
			fmt.Println("Test Passed: Error message displayed for invalid credentials")
		}
	}
}
